from flask import Flask, jsonify
from pymongo import MongoClient
import os

app = Flask(__name__)
mongo_host = os.getenv('MONGO_HOST', 'evaluation-mongo-1')
mongo_port = os.getenv('MONGO_PORT', '27017')
mongo_db= os.getenv('MONGO_DB', 'ynov')
mongo_uri = f"mongodb://{mongo_host}:{mongo_port}/"
# Configuration MongoDB
client = MongoClient(mongo_uri)
db = client[mongo_db]

@app.route('/movies')
def get_movies():
    mongo_coll_movies = os.getenv('MONGO_COLL', 'movies')
    collection = db[mongo_coll_movies]
    documents = collection.find({})
    
    resultat = [{"resultat":mongo_coll_movies}]
    for document in documents:
        # Assurez-vous d'adapter ceci à la structure de vos documents
        document['_id'] = str(document['_id'])  # Convertit ObjectId en string
        resultat.append(document)
    
    return jsonify(resultat)

@app.route('/grades')
def get_grades():
    mongo_coll_grades = os.getenv('MONGO_COLL', 'grades')
    collection = db[mongo_coll_grades]
    documents = collection.find({})
    
    resultat = [{"resultat":mongo_coll_grades}]
    for document in documents:
        # Assurez-vous d'adapter ceci à la structure de vos documents
        document['_id'] = str(document['_id'])  # Convertit ObjectId en string
        resultat.append(document)
    
    return jsonify(resultat)

if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True)